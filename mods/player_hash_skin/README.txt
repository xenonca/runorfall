Player Hash Skin Mod for Minetest
====================================

This mod calculates a hash code of a player's name and chooses a texture for him according to this hash code.
Eight default skins are shipped with the mod, you can add an arbitrary number of skins on your own by adding them to the textures folder. The simple_skins mod is supported and its skins will be used instead of the skins in the textures folder when it is detected.


-----------------------
original author: Hiradur
Code snippet taken from PilzAdam's player_textures mod.

License of textures:
--------------------------------
0-7: CC-BY-SA 3.0, original author: Jordach
added by Niwla:
8:   CC-BY-SA 3.0, original author: jojoa1997
9:   CC-BY-SA 3.0, original author: Evergreen
10:  CC-BY-SA 3.0, original author: philipbenr
11:  CC-BY-SA 3.0, original author: sdzen
12:  CC-BY-SA 3.0, original author: jordan4ibanez
13:  CC-BY-SA 3.0, original author: Rhys
