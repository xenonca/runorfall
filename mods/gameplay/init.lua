local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local spawnpos = {x=10,y=102,z=10}
local startpos = {x=10,y=27,z=10}
local players_ingame = {}

local open = io.open
local function read_file(path)
    local file = open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

local function write_file(path, content)
    local file = open(path, "wb") -- r read mode and b binary mode
    if not file then return nil end
    file:write(content)
    file:flush()
    file:close()
    return content
end

local function load_map(pos)
    worldedit.deserialize(pos, read_file(modpath .. "/schems/cube_1.we"))
end
local function load_spawn(pos)
    worldedit.deserialize(pos, read_file(modpath .. "/schems/spawn.we"))
end

local function get_ingame_count()
    local counter2 = 0
    for index in pairs(players_ingame) do
        counter2 = counter2 + 1
    end
    return counter2
end

local function startgame(name)
    local player_count2 = get_ingame_count()
    local admin = minetest.get_player_by_name(name)
    if player_count2 == 0 then
        minetest.chat_send_all(minetest.colorize("green","Game starts now!!! Be Ready!"))
        load_map({x=0,y=0,z=0})
        for id, player in ipairs(minetest.get_connected_players()) do -- Loop through all players online
            local start_hud = player:hud_add({
                hud_elem_type = "image",
                position      = {x = 0.5, y = 0.5},
                offset        = {x = 0,   y = 0},
                text          = "runorfall_game_starts_now.png",
                alignment     = {x = 0, y = 0},  -- center aligned
                scale         = {x = 1.5, y = 1.5}, -- covered late
                number = 0xD61818,
            })
            minetest.sound_play("runorfall_game_start", {
                to_player = player,
                gain = 2.0,
            })
            players_ingame[player:get_player_name()] = true
            minetest.after(2, function()
                player:set_pos(startpos)
            end)
            minetest.after(4, function()
                player:hud_remove(start_hud)
            end)
        end
    else
        minetest.chat_send_player(name, "Sorry, the game is already running, you can't start another one at the moment.")
    end
end

minetest.register_on_mapgen_init(function(mgparams)
	minetest.set_mapgen_setting("mg_name", "singlenode", true)
end)

local fall_on_timer = function(pos, elapsed)
	local n = minetest.get_node(pos)
	if not (n and n.name) then
		return true
	end
	-- Drop trap stone when player is nearby
	local objs = minetest.get_objects_inside_radius(pos, 1.1)
	for i, obj in pairs(objs) do
		if obj:is_player() then
            if n.name == "gameplay:fall" then
                minetest.after(0.6, function(pos)
                    minetest.set_node(pos, {name="gameplay:fall_2"})
                    minetest.check_for_falling(pos)
                end, pos)
            end
        return true
        end
	end
	return true
end

minetest.register_node("gameplay:side", {
    description = "Side Node",
    tiles = {"runorfall_side.png"},
    light_source = 15,
})

minetest.register_node("gameplay:start", {
    description = "Start Node",
    tiles = {"runorfall_start.png"},
    on_rightclick = function(pos, node, player, itemstack, pointed_thing)
        local playername = player:get_player_name()
        startgame(playername)
    end
})

minetest.register_node("gameplay:top", {
    description = "Top Node",
    tiles = {"^[colorize:#802BB1"},
    light_source = 15,
})

minetest.register_node("gameplay:bottom", {
    description = "Bottom Node",
    tiles = {"^[colorize:#802BB1"},
    light_source = 15,
})


minetest.register_node("gameplay:fall", {
    description = "Falling",
    tiles = {"runorfall_fall.png"},
    is_ground_content = false,
    on_construct = function(pos)
        minetest.get_node_timer(pos):start(0.1)
    end,
    light_source = 1,
    on_timer = fall_on_timer,
})

minetest.register_node("gameplay:fall_2", {
    description = "Falling",
    tiles = {"runorfall_fall.png"},
    is_ground_content = false,
    groups = {crumbly=3,cracky=3,falling_node=1,not_in_creative_inventory=1},
    drop = ""
})


minetest.register_abm{
    label = "falling remover",
    nodenames = {"group:falling_node"},
    interval = 1,
    chance = 1,
    action = function(pos)
        minetest.set_node(pos, {name = "air"})
    end,
}


minetest.register_on_joinplayer(function(player)
	player:set_physics_override(
        {
            speed = 1.3,
        }
    )
    player:set_pos({x=0,y=1200,z=25})
    minetest.after(2, function()
        player:set_pos(spawnpos)
    end)

    player:hud_set_flags({hotbar=false, healthbar=true, crosshair=true, wielditem=false, breathbar=true, minimap=false, minimap_radar=false})
end)




minetest.register_chatcommand("start", {
	params = "<text>",
	description = "Starts game",
	privs = {shout = true},
    func = function(name, text)
        startgame(name)
	end,
})

minetest.register_chatcommand("showhud", {
	params = "<text>",
	description = "Shows hudbar",
	privs = {server = true},
    func = function(name, text)
        local player = minetest.get_player_by_name(name)
        player:hud_set_flags({hotbar=true, healthbar=true, crosshair=true, wielditem=true, breathbar=true, minimap=true, minimap_radar=false})
	end,
})

minetest.register_chatcommand("hidehud", {
	params = "<text>",
	description = "Shows hudbar",
	privs = {server = true},
    func = function(name, text)
        local player = minetest.get_player_by_name(name)
        player:hud_set_flags({hotbar=false, healthbar=true, crosshair=true, wielditem=false, breathbar=true, minimap=false, minimap_radar=false})
	end,
})

minetest.register_chatcommand("savespawn", {
	params = "<text>",
	description = "Starts game",
	privs = {server = true},
    func = function(name, text)
        local content, count = worldedit.serialize({x=21,y=100,z=0}, {x=0,y=113,z=21})
        local path = modpath .. "/schems/spawn.we"
        os.remove(path)
        write_file(path, content)
	end,
})

--[[
minetest.register_chatcommand("savearena", {
	params = "<text>",
	description = "Saves arena",
	privs = {server = true},
    func = function(name, text)
        local content, count = worldedit.serialize({x=23,y=30,z=0}, {x=0,y=0,z=23})
        write_file(modpath .. "/schems/cube_1.we", content)
	end,
})
--]]

minetest.after(5, function()
    pos = {x=0,y=0,z=0}
    load_map(pos)
end)

minetest.after(1, function()
    pos = {x=0,y=100,z=0}
    load_spawn(pos)
end)


minetest.register_globalstep(function(dtime)
    local timer = 0
    if minetest.get_connected_players() == 0 then
        return -- Don't run the following code if no players are online
    end
    timer = timer + dtime
    if timer <= 5 then
        timer = 0
        for id, player in ipairs(minetest.get_connected_players()) do -- Loop through all players online
            if player:get_pos().y < 1 then
                local player_count = get_ingame_count()
                player:set_pos(spawnpos)
                players_ingame[player:get_player_name()] = nil
                if player_count - 1 == 0 then
                    minetest.chat_send_all(minetest.colorize("red", player:get_player_name() .. " won!"))
                else
                    if player_count - 1 == 1 then
                        minetest.chat_send_all(minetest.colorize("yellow", player:get_player_name() .. " died! ".. player_count -1 .. " player left."))
                    else
                        minetest.chat_send_all(minetest.colorize("yellow", player:get_player_name() .. " died! ".. player_count -1 .. " players left."))
                    end
                end
                minetest.add_particlespawner({
                    amount = 1,
                    time = 1,
                    minpos = {x=0, y=0, z=0},
                    maxpos = {x=0, y=0, z=0},
                    minvel = {x=0, y=0, z=0},
                    maxvel = {x=0, y=0, z=0},
                    minacc = {x=0, y=0, z=0},
                    maxacc = {x=0, y=0, z=0},
                    minexptime = 1,
                    maxexptime = 1,
                    minsize = 1,
                    maxsize = 1,
                    collisiondetection = false,
                    vertical = false,
                    texture = "runorfall_fall.png",
                })
            end
        end
    end
end)